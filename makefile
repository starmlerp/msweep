TARGET = main

CPPFLAGS = -lcurses -g

all: $(TARGET) tags

$(TARGET): %: %.cpp
	$(CXX) $(CPPFLAGS) -o $@ $^

tags: $(TARGET).cpp $(HEDS)
	ctags $^

.PHONY: cleanup
cleanup:
	rm $(TARGET) 

.PHONY: run
run: $(TARGET)
	./$^

.PHONY: debug
debug: $(TARGET)
	gdb $(TARGET)
