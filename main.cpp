#include <iostream>
#include <cstdlib>
#include <ctime>
#include <curses.h>

#define FLAG 'f'
#define FOG '#'
#define MINE 'm'

using namespace std;

const int difficulty[3][3] = {
	{9, 9, 10},
	{16, 16, 40},
	{16, 30, 99}
};

struct square{
	int scout = 0;
	bool fog = true;
	bool flag = false;
	bool bomb = false;
}**minefield;

int w, h, b, fog, flags = 0;
bool automode = false;

void genfield(){
	for(int i = 0; i < b; i++){
		int x, y;
		do{
			x = rand() % w;
			y = rand() % h;
		}while(minefield[x][y].bomb);
		minefield[x][y].bomb = true;
	}
	for(int x = 0; x < w; x++){
		for(int y = 0; y < h; y++){
			for(int i = x - 1; i <= x + 1; i++){
				for(int j = y - 1; j <= y + 1; j++){
					if(i < 0 || i >= w) continue;
					if(j < 0 || j >= h) continue;
					minefield[x][y].scout += minefield[i][j].bomb;
				}
			}
				
		}
	}
}

void printfield(char *text, char indicator){
	clear();
	mvprintw(0, 0, "flags: %d", b-flags);
	mvprintw(w+1, 0, "%s\npress any key to continue", text);
		for(int i = 0; i < w; i++){
		for(int j = 0; j < h; j++){
			if(minefield[i][j].bomb){
				attron(COLOR_PAIR(10));
				mvaddch(i+1, j, indicator);
				attroff(COLOR_PAIR(10));
			
			}
			else if(!minefield[i][j].scout)mvaddch(i+1, j, '.');
			else{
				attron(COLOR_PAIR(minefield[i][j].scout));
				mvaddch(i+1, j, '0' + minefield[i][j].scout);
				attroff(COLOR_PAIR(minefield[i][j].scout));
			}
		}
	}
	refresh();
	getch();
	endwin();
}

int reveal(int x, int y){
	if(minefield[x][y].flag)return 0;
	int regen = 0;
	while(minefield[x][y].bomb && fog == h * w){
		mvprintw(w+1, 0, "regenerating: %d", ++regen);
		genfield();
	}
	if(minefield[x][y].bomb){
		printfield("you stepped on a mine!", MINE);
		return 1;
	}
	if(!minefield[x][y].fog && minefield[x][y].scout && automode){
		int flagged = 0;
		for(int i = x - 1; i <= x + 1; i++)
			for(int j = y - 1; j <= y + 1; j++)
				if(i >= 0 && i < w && j >= 0 && j < h)flagged += (int)minefield[i][j].flag;
		if(minefield[x][y].scout == flagged)
			for(int i = x - 1; i <= x + 1; i++)
				for(int j = y - 1; j <= y + 1; j++)
					if(i >= 0 && i < w && j >= 0 && j < h)
						if(!minefield[i][j].flag && minefield[i][j].fog)
							if(reveal(i, j))return 1;
		flagged = 0;
		for(int i = x - 1; i <= x + 1; i++)
			for(int j = y - 1; j <= y + 1; j++)
				if(i >= 0 && i < w && j >= 0 && j < h)flagged += (int)minefield[i][j].fog;
		if(minefield[x][y].scout == flagged)
			for(int i = x - 1; i <= x + 1; i++)
				for(int j = y - 1; j <= y + 1; j++)
					if(i >= 0 && i < w && j >= 0 && j < h)
						if(!minefield[i][j].flag && minefield[i][j].fog){
							minefield[i][j].flag = true;
							flags--;
						}

	}
	if(!minefield[x][y].fog)return 0;
	minefield[x][y].fog = false;
	fog--;

	for(int i = x - 1; i <= x + 1; i++){
		for(int j = y - 1; j <= y + 1; j++){
			if(i < 0 || i >= w) continue;
			if(j < 0 || j >= h) continue;
			if(!minefield[x][y].scout && minefield[i][j].fog && minefield[i][j].scout){
				minefield[i][j].fog = false;
				fog--;
			}
			if(!minefield[i][j].scout && !(i == x && j == y) && minefield[i][j].fog)
				if(reveal(i, j))return 1;
		}
	}
	return 0;
}

int main(int argc, char *argv[]){
	srand(time(0));
	int d = 1;
	if(argc > 1){
		for(int i = 1; i < argc; i++){
			if(argv[i][0] != '-'){
				cerr<<"option expected"<<endl;
				return 1;
			}
			switch(argv[i][1]){
				case 'w':
					w = atoi(argv[++i]);
				break;
				case 'h':
					h = atoi(argv[++i]);
				break;
				case 'b':
					b = atoi(argv[++i]);
				break;
				case 'd':{
					d = atoi(argv[++i])-1;
//					i = argc;
				}break;
				case 'a':
					automode = true;
//					i--;
				break;
				default:
					 cerr<<"unkown option "<<argv[i]<<endl;
					 return 1;
			}
		}
	}
	w = difficulty[d][0];
	h = difficulty[d][1];
	b = difficulty[d][2];
	
	minefield = new square*[w];
	for(int i = 0; i < w; i++)
		minefield[i] = new square[h];
	
	genfield();

	initscr();
	start_color();
	cbreak();
	noecho();
	curs_set(0);
	intrflush(stdscr, FALSE);
	keypad(stdscr, TRUE);
//	nodelay(stdscr, TRUE);

	if(LINES < h + 3 || COLS > w){
		cerr<<"need at least "<<w<<"x"<<h + 3<<" terminal window to render screen"<<endl;
	}

	init_pair(1, COLOR_BLUE,      COLOR_BLACK);
	init_pair(2, COLOR_GREEN,     COLOR_BLACK);
	init_pair(3, COLOR_RED,       COLOR_BLACK);
	init_pair(4, COLOR_MAGENTA,   COLOR_BLACK);
	init_pair(5, COLOR_YELLOW,    COLOR_BLACK);
	init_pair(6, COLOR_CYAN,      COLOR_BLACK);
	init_pair(7, COLOR_RED,       COLOR_BLACK);
	init_pair(8, COLOR_RED,       COLOR_BLACK);

	init_pair(9, COLOR_BLACK, COLOR_WHITE); // cursor
	init_pair(10, COLOR_BLACK, COLOR_RED); // flag / mine


	fog = w*h;
	int x = w / 2;
	int y = h / 2;
	while(fog > b){
		clear();
		mvprintw(0, 0, "flags: %d", b-flags);
		if(minefield[x][y].fog)
			mvprintw(w+1, 0, "fog of war");
		else if(minefield[x][y].flag)
			mvprintw(w+1, 0, "a flag you placed");
		else 
			mvprintw(w+1, 0, "this field has %d \nmines surrounding it", minefield[x][y].scout);

		for(int i = 0; i < w; i++){
			for(int j = 0; j < h; j++){
				attroff(COLOR_PAIR(9));
				if(x == i && y == j)attron(COLOR_PAIR(9));
				if(minefield[i][j].flag){
					if(x != i || y != j)attron(COLOR_PAIR(10));
					mvaddch(i+1, j, FLAG);
					attroff(COLOR_PAIR(10));
					continue;
				}
				if(minefield[i][j].fog){
					mvaddch(i+1, j, FOG);
					continue;
				}

				if(!minefield[i][j].scout)mvaddch(i+1, j, '.');
				else{
					if(x != i || y != j)attron(COLOR_PAIR(minefield[i][j].scout));
					mvaddch(i+1, j, '0' + minefield[i][j].scout);
					attroff(COLOR_PAIR(minefield[i][j].scout));
				}
			}
		}
		refresh();
		
		switch(getch()){
			case 'w': x--; break;
			case 's': x++; break;
			case 'a': y--; break;
			case 'd': y++; break;
			case 'e':{
				if(reveal(x, y))return 0;
			}break;
			case 'q':
				if(flags > b)break;
				if(!minefield[x][y].fog)break;
				minefield[x][y].flag = !minefield[x][y].flag;
				if(minefield[x][y].flag)flags++;
				else flags--;
			break;
		}
		x = (w+x) % w;
		y = (h+y) % h;
	}
	printfield("you cleared out the field!", FLAG);
	endwin();
	return 0;
}

